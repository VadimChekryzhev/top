$(document).ready(function () {

  $('.owl-carousel').owlCarousel({
    // navigation : true,
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem : true
  });

  // $('.s-item').matchHeight();

  $('.item-1, .item-2, .item-3,' +
    '.item-4, .item-5, item-6').matchHeight();
});